#include <stdio.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <inttypes.h>
#include <sys/timeb.h>
#include "stack.h"
#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

#define max3(x, y, z) (((x) > (y)) ? (((x) > (z)) ? (x) : (z)) : (((y) > (z)) ? (y) : (z)))
#define min3(x, y, z) (((x) < (y)) ? (((x) < (z)) ? (x) : (z)) : (((y) < (z)) ? (y) : (z)))

typedef struct Array {
    int* array;
    int size;
} Array;
typedef struct pair {
    int first;
    int second;
} pair;
typedef struct ListNode {
    int data;
    struct ListNode* next;
} ListNode;
typedef struct stack stack;

int gcd(int a, int b);
double log_base(double x, int base);
void swap(int* x, int* y);
void quick_sort(int* array, size_t array_size);
void insertion_sort(int* array, size_t array_size);
void counting_sort(int* array, size_t array_size, size_t counter_size);
int* merge(int* a, int* b, size_t a_size, size_t b_size);
void merge_sort(int* array, size_t array_size);
int to_int(const char* string);
bool is_prime(int x);
int amaxe(int* array, size_t array_size);
int amine(int* array, size_t array_size);
int amaxi(int* array, size_t array_size);
int amini(int* array, size_t array_size);
void aprint(int* array, size_t array_size);
void acopy(int* source, int* destination, size_t size);
Array get_divisors(int x);
void areverse(int* array, size_t array_size);
int64_t asum(int* array, size_t array_size);
double amean(int* array, size_t array_size);
double amedian(int* array, size_t array_size);
int lower_bound(int* array, size_t array_size, int value);
int upper_bound(int* array, size_t array_size, int value);
pair equal_range(int* array, size_t array_size, int value);
bool is_sorted(int* array, size_t array_size, bool reversed);


int main() {
}


int gcd(int a, int b) {
    if (b == 0) return a;
    return gcd(b, a % b);
}

double log_base(double x, int base) {
    return log(x) / log(base);
}

void swap(int *x, int *y) {
    int temp = *x;
    *x = *y;
    *y = temp;
}

void quick_sort(int* a, size_t n) {
    if (n <= 1) return;
    int pivot = a[0];
    int* left = (int*)malloc(sizeof(int) * n);
    int* equal = (int*)malloc(sizeof(int) * n);
    int* right = (int*)malloc(sizeof(int) * n);
    int lp = 0, ep = 0, rp = 0;
    for (size_t i = 0; i < n; i++)
    {
        if (a[i] < pivot)
            left[lp++] = a[i];
        else if (a[i] > pivot)
            right[rp++] = a[i];
        else
            equal[ep++] = a[i];
    }
    quick_sort(left, lp);
    quick_sort(right, rp);
    int k = 0;
    for (int i = 0; i < lp; i++)
        a[k++] = left[i];
    for (int i = 0; i < ep; i++)
        a[k++] = equal[i];
    for (int i = 0; i < rp; i++)
        a[k++] = right[i];
    free(left), free(equal), free(right);
}

void insertion_sort(int* a, size_t n) {
    for (size_t i = 1; i < n; i++) {
        size_t k = i;
        while (k > 0 && a[k - 1] > a[k]) {
            swap(&a[k - 1], &a[k]);
            k--;
        }
    }
}

void counting_sort(int* a, size_t n, size_t c_size) {
    int* counter = (int*)calloc(sizeof(int), c_size+1);
    for (size_t i = 0; i < n; i++)
        counter[a[i]]++;
    int k = 0;
    for (size_t i = 0; i <= c_size; i++)
        for (int j = 0; j < counter[i]; j++)
            a[k++] = i;
    free(counter);
}

int* merge(int* a, int* b, size_t a_size, size_t b_size) {
    int n = a_size + b_size;
    int* out = (int*)malloc(sizeof(int) * n);
    int ap = 0, bp = 0, i = 0;
    while (ap < a_size && bp < b_size) {
        if (a[ap] <= b[bp]) {
            out[i++] = a[ap++];
        } else {
            out[i++] = b[bp++];
        }
    }
    while (ap < a_size)
        out[i++] = a[ap++];
    while (bp < b_size)
        out[i++] = b[bp++];
    return out;
}
void merge_sort(int* a, size_t n) {
    if (n <= 1) return;
    int middle = n/2;
    int* l = (int*)malloc(sizeof(int) * middle);
    int* r = (int*)malloc(sizeof(int) * (middle + 1));
    int ap = 0, l_size = 0, r_size = 0;
    while (ap < middle)
        l[l_size++] = a[ap++];
    while (ap < n)
        r[r_size++] = a[ap++];
    merge_sort(l, l_size);
    merge_sort(r, r_size);
    int* c = (int*)malloc(sizeof(int) * n);
    c = merge(l, r, l_size, r_size);
    free(l), free(r);
    for (int i = 0; i < n; i++) {
        a[i] = c[i];
    }
    free(c);
}

int to_int(const char* string) {
    int out = 0;
    for (size_t i = 0; i < strlen(string); i++)
        out = out*10 + (string[i] - '0');
    return out;
}

bool is_prime(int x) {
    for (int i = 2; i <= sqrt(x); i++)
        if (x % i == 0) return false;
    return true;
}

int amaxe(int* a, size_t n) {
    int m = INT32_MIN;
    for (size_t i = 0; i < n; i++)
        m = max(m, a[i]);
    return m;
}

int amine(int* a, size_t n) {
    int m = INT32_MAX;
    for (size_t i = 0; i < n; i++)
        m = min(m, a[i]);
    return m;
}

int amaxi(int* a, size_t n) {
    int m = INT32_MIN;
    int out = -1;
    for (size_t i = 0; i < n; i++)
        if (a[i] > m) {
            m = a[i];
            out = i;
        }
    return out;
}

int amini(int* a, size_t n) {
    int m = INT32_MAX;
    int out = -1;
    for (size_t i = 0; i < n; i++)
        if (a[i] < m) {
            m = a[i];
            out = i;
        }
    return out;
}

void aprint(int* a, size_t n) {
    for (size_t i = 0; i < n; i++) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

void acopy(int* s, int* d, size_t n) {
    for (int i = 0; i < n; i++)
        d[i] = s[i];
}

struct Array get_divisors(int x) {
    int* divisors = (int*)malloc(sizeof(int)*((int)sqrt(x) + 1));
    int k = 0;
    for (int i = 2; i <= sqrt(x); i++) {
        if (x % i == 0) {
            divisors[k++] = i;
            divisors[k++] = x/i;
        }
    }
    Array out = {divisors, k};
    return out;
}

void areverse(int* a, size_t n) {
    for (size_t i = 0; i < n/2; i++)
        swap(&a[i], &a[n-1-i]);
}

int64_t asum(int* a, size_t n) {
    int64_t sum = 0;
    for (int i = 0; i < n; i++)
        sum += a[i];
    return sum;
}

double amean(int* a, size_t n) {
    return asum(a, n) / (double)n;
}
double amedian(int* a, size_t n) {
    int* tmp = (int*)malloc(sizeof(int) * n);
    acopy(a, tmp, n);
    merge_sort(tmp, n);
    if (n % 2 == 1) {
        double out = tmp[n/2];
        free(tmp);
        return out;
    }
    double out = (tmp[n/2] + tmp[n/2+1]) / 2.;
    free(tmp);
    return out;
}

int lower_bound(int* a, size_t n, int x) {
    int l = 0, r = n;
    while (l < r) {
        int m = l + (r-l)/2;
        if (x <= a[m])
            r = m;
        else
            l = m + 1;
    }
    return l;
}

int upper_bound(int* a, size_t n, int x) {
    int l = 0, r = n;
    while (l < r) {
        int m = l + (r-l)/2;
        if (x >= a[m])
            l = m + 1;
        else
            r = m;
    }
    return l;
}

pair equal_range(int* a, size_t n, int x) {
    pair out = {lower_bound(a, n, x), upper_bound(a, n, x)};
    return out;
}

bool is_sorted(int* a, size_t n, bool reversed) {
    int sign = 1;
    if (reversed) sign = -1; 
    for (int i = 1; i < n; i++) {
        if (sign*a[i-1] > sign*a[i]) return false;
    }
    return true;
}