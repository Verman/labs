#include <stdlib.h>
#include <inttypes.h>
#include "stack.h"

struct stack {
    int top;
    unsigned capacity;
    int* array;
};

struct stack* create_stack(unsigned capacity) {
    struct stack* s = (struct stack*)malloc(sizeof(struct stack));
    s->capacity = capacity;
    s->top = -1;
    s->array = (int*)malloc(s->capacity * sizeof(int));
    return s;
}

int is_full(struct stack* s) {
    return s->top == s->capacity -1;
}

int is_empty(struct stack* s) {
    return s->top == -1;
}

void push(struct stack* s, int value) {
    if (is_full(s)) return;
    s->array[++s->top] = value;
}

int pop(struct stack* s) {
    if (is_empty(s)) return INT32_MIN;
    return s->array[s->top--];
}

int top(struct stack* s) {
    if (is_empty(s)) return INT32_MIN;
    return s->array[s->top];
}