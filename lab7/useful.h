#include <stddef.h>
#ifndef USEFUL_H
#define USEFUL_H

#define max(x, y) (((x) > (y)) ? (x) : (y))
#define min(x, y) (((x) < (y)) ? (x) : (y))
#define max3(x, y, z) (((x) > (y)) ? (((x) > (z)) ? (x) : (z)) : (((y) > (z)) ? (y) : (z)))
#define min3(x, y, z) (((x) < (y)) ? (((x) < (z)) ? (x) : (z)) : (((y) < (z)) ? (y) : (z)))

// INPUT NUMBERS
void InputDouble(double* input);
void InputInt(int* input);

// ARRAY INITIALIZATION
void InitArrayDouble(double* array, size_t size);
void InitArrayInt(int* array, size_t size);
void InitArrayDoubleRandom(double* array, size_t size, double min, double max);
void InitArrayIntRandom(int* array, size_t size, int min, int max);

// ARRAY FUNCTIONS
 // DOUBLE ARRAYS
void PrintArrayDouble(double* array, size_t size);
double MinElemArrayDouble(double* array, size_t size);
double MaxElemArrayDouble(double* array, size_t size);
int MinElemPosArrayDouble(double* array, size_t size);
int MaxElemPosArrayDouble(double* array, size_t size);
 // INTEGER ARRAYS
void PrintArrayInt(int* array, size_t size);
int MinElemArrayInt(int* array, size_t size);
int MaxElemArrayInt(int* array, size_t size);
int MinElemPosArrayInt(int* array, size_t size);
int MaxElemPosArrayInt(int* array, size_t size);

// SORTINGS
void QuickSortArrayDouble(double* array, size_t n);
void QuickSortArrayInt(int* array, size_t n);
#endif