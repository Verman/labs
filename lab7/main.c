#include "useful.h"
#include <stdio.h>

#define ARRAY_MAX_SIZE 200

int main() {
    // INPUT TEST
    int a;
    double b;
    printf("Enter integer:\n");
    InputInt(&a);
    printf("Enter double:\n");
    InputDouble(&b);
    printf("INT = %d  DOUBLE = %lf\n", a, b);

    // INIT ARRAY TEST
    int c_size, d_size;

    int c[ARRAY_MAX_SIZE];
    double d[ARRAY_MAX_SIZE];

    do {
        printf("Enter c_size:\n");
        InputInt(&c_size);
    } while (c_size > ARRAY_MAX_SIZE || c_size < 0);

    do {
        printf("Enter d_size:\n");
        InputInt(&d_size);
    } while (d_size > ARRAY_MAX_SIZE || d_size < 0);

    InitArrayInt(c, c_size);
    PrintArrayInt(c, c_size);
    InitArrayDouble(d, d_size);
    PrintArrayDouble(d, d_size);

    // RANDOM INIT TEST
    int int_min, int_max;
    double double_min, double_max;

    printf("Enter min for int:\n");
    InputInt(&int_min);
    printf("Enter max for int:\n");
    InputInt(&int_max);
    InitArrayIntRandom(c, c_size, int_min, int_max);
    PrintArrayInt(c, c_size);
    QuickSortArrayInt(c, c_size);
    PrintArrayInt(c, c_size);

    printf("Enter min for double:\n");
    InputDouble(&double_min);
    printf("Enter max for double:\n");
    InputDouble(&double_max);
    InitArrayDoubleRandom(d, d_size, double_min, double_max);
    PrintArrayDouble(d, d_size);
    QuickSortArrayDouble(d, d_size);
    PrintArrayDouble(d, d_size);
}