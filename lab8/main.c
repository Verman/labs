#include <stddef.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "useful.h"

int LocalMinimumCounter(int* array, size_t size);
void SortOnlyEvenPos(int* array, size_t size);
int MinOfLocalMax(int* array, size_t size);
void SortOnlyEven(int* array, size_t size);
int AscendingSectorsCounter(int* array, size_t size);
int* Merge2Arrays(int* array1, int* array2, size_t size1, size_t size2);

#define A 15
#define B 5

int main() {
    int a[A] = {60, 63, 73, 49, 24, 52, 33, 65, 96, 46, 84, 84, 84, 58, 33},
    b[B] = {30, 80, 50, 90, 0};
    srand(time(0));
    InitArrayIntRandom(a, A, 25, 100);
    InitArrayIntRandom(b, B, 0, 75);
    PrintArrayInt(a, A);
    PrintArrayInt(b, B);
    // LocalMinimumCounter
    printf("%d\n", LocalMinimumCounter(a, A));
    printf("%d\n", LocalMinimumCounter(b, B));
    // MinOfLocalMax
    printf("%d\n", MinOfLocalMax(a, A));
    printf("%d\n", MinOfLocalMax(b, B));
    // Merge
    int* m = Merge2Arrays(a, b, A, B);
    PrintArrayInt(m, A + B);
    //
    SortOnlyEven(a, A);
    SortOnlyEvenPos(b, B);
    PrintArrayInt(a, A);
    PrintArrayInt(b, B);
}

int LocalMinimumCounter(int* a, size_t n) {
    int c = 0;
    for (int i = 0; i < n; i++) {
        if (i == 0 && a[i + 1] > a[i]) c++;
        else if (i == n - 1 && a[i - 1] > a[i]) c++;
        else if (a[i - 1] > a[i] && a[i + 1] > a[i]) c++;
    }
    return c;
}

void SortOnlyEvenPos(int* a, size_t n) {
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            if (i % 2 == 0 && j % 2 == 0 && a[i] > a[j]) {
                int tmp = a[i];
                a[i] = a[j];
                a[j] = tmp;
            }
        }
    }
}

int MinOfLocalMax(int* a, size_t n) {
    int* tmp = (int*)malloc(sizeof(int) * n);
    int p = 0;
    for (int i = 0; i < n; i++) {
        if (i == 0 && a[i + 1] < a[i]) tmp[p++] = a[i];
        else if (i == n - 1 && a[i - 1] < a[i]) tmp[p++] = a[i];
        else if (a[i - 1] < a[i] && a[i + 1] < a[i]) tmp[p++] = a[i];
    }
    int m = INT_MAX;
    for (int i = 0; i < p; i++) {
        m = min(m, tmp[i]);
    }
    free(tmp);
    return m;
}

void SortOnlyEven(int* a, size_t n) {
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            if (a[i] % 2 == 0 && a[j] % 2 == 0 && a[i] > a[j]) {
                int tmp = a[i];
                a[i] = a[j];
                a[j] = tmp;
            }
        }
    }
}

int AscendingSectorsCounter(int* a, size_t n) {
    int c = 0;
    int tmp = 0;
    for (int i = 1; i < n; i++) {
        if (a[i] >= a[i - 1]) tmp = 1;
        if ((tmp && a[i] < a[i - 1]) || (tmp && i == n - 1)) {
            c++;
            tmp = 0;
        }
    }
    return c;
}

int* Merge2Arrays(int* a, int* b, size_t an, size_t bn) {
    int* out = (int*)malloc(sizeof(int) * (an + bn));
    int ap = 0, bp = 0, op = 0;
    while (ap < an && bp < bn) {
        if (a[ap] < b[bp]) {
            out[op++] = a[ap++];
        } else {
            out[op++] = b[bp++];
        }
    }
    for (int i = ap; i < an; i++) {
        out[op++] = a[i];
    }
    for (int i = bp; i < bn; i++) {
        out[op++] = b[i];
    }
    return out;
}