#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <time.h>
#include <inttypes.h>
#include <float.h>
#include <limits.h>
#include <assert.h>
#include "useful.h"

#define max(x, y) (((x) > (y)) ? (x) : (y))
#define min(x, y) (((x) < (y)) ? (x) : (y))
#define max3(x, y, z) (((x) > (y)) ? (((x) > (z)) ? (x) : (z)) : (((y) > (z)) ? (y) : (z)))
#define min3(x, y, z) (((x) < (y)) ? (((x) < (z)) ? (x) : (z)) : (((y) < (z)) ? (y) : (z)))

// INPUT NUMBERS
void InputDouble(double* input) {
    int flag;
    do {
        int ch; flag = 0;
        int count = scanf("%lf", input);
        while ((ch = getchar()) != '\n');
        if (count < 1) {
            puts("Error, try again");
            flag = 1;
        }
    } while (flag);
}

void InputInt(int* input) {
    int flag;
    do {
        int ch; flag = 0;
        int count = scanf("%d", input);
        while ((ch = getchar()) != '\n');
        if (count < 1) {
            puts("Error, try again");
            flag = 1;
        }
    } while (flag);
}

// ARRAY INITIALIZATION
void InitArrayDouble(double* a, size_t n) {
    for (int i = 0; i < n; i++) {
        printf("Enter the %d-th element: ", i);
        InputDouble(a + i);
    }
}

void InitArrayInt(int* a, size_t n) {
    for (int i = 0; i < n; i++) {
        printf("Enter the %d-th element: ", i);
        InputInt(a + i);
    }
}

void InitArrayDoubleRandom(double* a, size_t n, double l, double r) {
    assert(r > l);
    srand(time(0));
    for (int i = 0; i < n; i++) {
        a[i] = l + (rand() / (RAND_MAX / (r - l)));
    }
}

void InitArrayIntRandom(int* a, size_t n, int l, int r) {
    assert(r >= l);
    srand(time(0));
    for (int i = 0; i < n; i++) {
        a[i] = l + rand() % (r - l + 1);
    }
}

// ARRAY FUNCTIONS
 // DOUBLE ARRAYS
void PrintArrayDouble(double* a, size_t n) {
    for (int i = 0; i < n; i++) {
        printf("%lf ", a[i]);
    }
    printf("\n");
}

double MinElemArrayDouble(double* a, size_t n) {
    double m = DBL_MAX;
    for (int i = 0; i < n; i++)
        m = min(m, a[i]);
    return m;
}

double MaxElemArrayDouble(double* a, size_t n) {
    double m = DBL_MIN;
    for (int i = 0; i < n; i++)
        m = max(m, a[i]);
    return m;
}

int MinElemPosArrayDouble(double* a, size_t n) {
    double m = DBL_MAX;
    int out = -1;
    for (int i = 0; i < n; i++)
        if (a[i] < m) {
            m = a[i];
            out = i;
        }
    return out;
}

int MaxElemPosArrayDouble(double* a, size_t n) {
    double m = DBL_MIN;
    int out = -1;
    for (int i = 0; i < n; i++)
        if (a[i] > m) {
            m = a[i];
            out = i;
        }
    return out;
}

int FindElemPosArrayDouble(double* a, size_t n, double x) {
    for (int i = 0; i < n; i++) {
        if (a[i] == x) return i;
    }
    return -1;
}
 // INTEGER ARRAYS
void PrintArrayInt(int* a, size_t n) {
    for (int i = 0; i < n; i++) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

int MinElemArrayInt(int* a, size_t n) {
    int m = INT32_MAX;
    for (int i = 0; i < n; i++)
        m = min(m, a[i]);
    return m;
}

int MaxElemArrayInt(int* a, size_t n) {
    int m = INT32_MIN;
    for (int i = 0; i < n; i++)
        m = max(m, a[i]);
    return m;
}

int MinElemPosArrayInt(int* a, size_t n) {
    int m = INT32_MAX;
    int out = -1;
    for (int i = 0; i < n; i++)
        if (a[i] < m) {
            m = a[i];
            out = i;
        }
    return out;
}

int MaxElemPosArrayInt(int* a, size_t n) {
    int m = INT32_MIN;
    int out = -1;
    for (int i = 0; i < n; i++)
        if (a[i] > m) {
            m = a[i];
            out = i;
        }
    return out;
}

int FindElemPosArrayInt(int* a, size_t n, int x) {
    for (int i = 0; i < n; i++) {
        if (a[i] == x) return i;
    }
    return -1;
}


// SORTINGS
void QuickSortArrayDouble(double* a, size_t n) {
    if (n <= 1) return;
    double pivot = a[0];
    double* left = (double*)malloc(sizeof(double) * n);
    double* equal = (double*)malloc(sizeof(double) * n);
    double* right = (double*)malloc(sizeof(double) * n);
    int lp = 0, ep = 0, rp = 0;
    for (int i = 0; i < n; i++)
    {
        if (a[i] < pivot)
            left[lp++] = a[i];
        else if (a[i] > pivot)
            right[rp++] = a[i];
        else
            equal[ep++] = a[i];
    }
    QuickSortArrayDouble(left, lp);
    QuickSortArrayDouble(right, rp);
    int k = 0;
    for (int i = 0; i < lp; i++)
        a[k++] = left[i];
    for (int i = 0; i < ep; i++)
        a[k++] = equal[i];
    for (int i = 0; i < rp; i++)
        a[k++] = right[i];
    free(left), free(equal), free(right);
}

void QuickSortArrayInt(int* a, size_t n) {
    if (n <= 1) return;
    int pivot = a[0];
    int* left = (int*)malloc(sizeof(int) * n);
    int* equal = (int*)malloc(sizeof(int) * n);
    int* right = (int*)malloc(sizeof(int) * n);
    int lp = 0, ep = 0, rp = 0;
    for (int i = 0; i < n; i++)
    {
        if (a[i] < pivot)
            left[lp++] = a[i];
        else if (a[i] > pivot)
            right[rp++] = a[i];
        else
            equal[ep++] = a[i];
    }
    QuickSortArrayInt(left, lp);
    QuickSortArrayInt(right, rp);
    int k = 0;
    for (int i = 0; i < lp; i++)
        a[k++] = left[i];
    for (int i = 0; i < ep; i++)
        a[k++] = equal[i];
    for (int i = 0; i < rp; i++)
        a[k++] = right[i];
    free(left), free(equal), free(right);
}