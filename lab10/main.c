#include <stdio.h>
#include <string.h>

typedef long long ll;

ll fib(int i) {
    if (i < 2) {
        return 1;
    }
    return fib(i - 1) + fib(i - 2);
};

ll sumFib1(ll m, ll sum) {
    int i = 0;
    while (1) {
        ll f = fib(i++);
        if (sum + f > m) return sum;
        sum += f;
    }
}

ll sumFib2(ll m, ll sum, int first, int second) {
    if (first + second > m) return sum;
    return sumFib2(m, sum + first, second, first + second);
}

int main() {
    printf("%lli\n", sumFib2(1000000000, 0, 1, 1));
}