#include <iostream>

typedef long long ll;

ll sumFib(ll m, ll sum = 0, int first = 1, int second = 1) {
    if (first + second > m) return sum;
    return sumFib(m, sum + first, second, first + second);
}

int main() {
    std::cout << sumFib(1000000000) << '\n';
}