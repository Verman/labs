#ifndef STACK_H
#define STACK_H

struct stack;

struct stack* create_stack(unsigned capacity);
int is_full(struct stack* s);
int is_empty(struct stack* s);
void push(struct stack* s, int value);
int pop(struct stack* s);
int top(struct stack* s);

#endif